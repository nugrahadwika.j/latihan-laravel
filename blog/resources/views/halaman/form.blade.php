<!DOCTYPE html>
<html>
  <head>
    <title>form</title>
  </head>
  <body>
    <h2>Buat Account Baru!</h2>
    <h3>Sign Up Form</h3>

    <!--membuat form-->
    <form action="/kirim" method="post">
    @csrf
      <label for="nama depan">First name:</label><br /><br />
      <input type="text" name="namadepan" /><br /><br />
      <label for="nama belakang">Last name:</label><br /><br />
      <input type="text" name="namabelakang" />
      <br /><br />

      <!--membuat radio button-->
      <label>Gender:</label><br />
      <input type="radio" name="gender" value="L" checked />Male <input type="radio" name="gender" value="P" checked />Female <input type="radio" name="gender" value="O" checked />Other <br /><br />

      <!--membuat option-->
      <label>Nationality:</label><br /><br />
      <select>
        <option value="Indonesia">Indonesia</option>
        <option value="Amerika">Amerika</option>
        <option value="Inggris">Inggris</option>
        <br /><br />
      </select>
      <br /><br />

      <!--membuat check button -->
      <label>Language Spoken:</label><br /><br />
      <input type="checkbox" name="bahasa" value="Bahasa Indonesia" /> Bahasa Indonesia <br />
      <input type="checkbox" name="bahasa" value="English" /> English<br />
      <input type="checkbox" name="bahasa" value="Others" /> Other<br />
      <br />

      <!--membuat textarea-->
      <label>Bio:</label><br /><br />
      <textarea cols="30px" rows="5px"></textarea>
      <br /><br />

      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
