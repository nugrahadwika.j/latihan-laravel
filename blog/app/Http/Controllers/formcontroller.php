<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class formcontroller extends Controller
{
    public function form(){
        return view('halaman.form');
    }

    public function send(Request $request){
        $nama1 = $request->namadepan;
        $nama2 = $request->namabelakang;

        return view ('halaman.home' , compact('nama1' , 'nama2'));
    }

    
}

